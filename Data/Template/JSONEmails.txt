﻿{
	"emails": {
		"accessError": {
			"subject": "@appname Website is not Responsive",
			"body": "<p> There has been an unexpected error trying to access @appname Website at </p> <a>href='@url'</a> <p><br><br>Please, vaidate the Website is accessible.<br><br>Thank you.</p>",
			"ishtml": false,
			"wildcards": [
				"@appname",
				"@url"
			]
		},
		"credentialsError": {
			"subject": "@appname Credentials are not Valid",
			"body": "<p>Hello,<br><br>It was not possible to access @appname at: <a>@url</a> with the provided Credentials.<br><br></p><p>Please,validate credentials are updated.<br><br>Thank you.<br><br></p>",
			"ishtml": true,
			"wildcards": [
				"@appname",
				"@url"
			]
		},
		"unexpectedError": {
			"subject": "Unexpected Error",
			"body": "There has been an unexpected error while running the process.\r\n\r\nPlease, send an email to the RPA Support Team at rpa.support@careanalytics.com to validate the error cause.\r\n\r\nThank you.",
			"ishtml": false,
			"wildcards": []
		},
		"noDataAvailable": {
			"subject": "Patients not Available in VaultMR",
			"body": "<p>Hello,<br><br> When attempting to review active patient charts for diagnosis documentation,we found no patients in Admitted status in Vault MR.<br><br>Please, ensure any currently admitted patients have a patients status equal to 'Admitted' in Vault MR.<br><br><br>Thank you.</p>",
			"ishtml": true,
			"wildcards": []
		},
			"finalReport": {
			"subject": "Final Report",
			"body": "The VaultMR process complete.\r\n\r\nThank you.",
			"ishtml": false,
			"wildcards": []
		}
	},
	"signature": "Sent by,\r\nAutomated Process\r\nAttended by Optimus Prime \r\nReply back to: Optimus.Prime@VMR.com\r\n http://www.VMR.com\r\n VaultMR.",
	"signatureHtml": "<p>Sent by,<br>Automated Process<br>Attended by Optimus Prime<br>Reply back to: Optimus.Prime@VMR.com <br> <a>http://www.VMR.com\r\n</a></p>"
}